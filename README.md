Operasi kondisi

==, !=, >=, <=, >, <, %

struktur if

if (kondisi) {
	// di eksekusi jika kondisi true
}

if (kondisi) {
	// di eksekusi jika kondisi true
} else {
	// di eksekusi jika kondisi false
}

if (kondisi) {
	// di eksekusi jika kondisi true
} else if (kondisiDua){
	// di eksekusi jika kondisi pertama false, cek kondisi kedua
} else {
	// jika kondisi pertama dan kedua false
}


for loop


array.forEach {
	println(it)
}

for (fruit in fruits) {
	println(fruit)
}

for (i in 1..10) {
	println(i)
}

// size 10
// show index 0 - 9
for (i in 0 until fruits.size) {
	println(fruits[i]
}
// perulangan 1..10 kelipatan 2
for (i in 1..10 step 2) {
	println(i)
}
// perulangan menurun
for (i in 10 downTo 1) {
	println(i)
}
// perulangan dengan index dan value
for ((i, fruit) in fruits.withIndex()) {
	println("index $i : $fruit")
}

// WHEN 
    val userSelect = "Academy"
    when(userSelect) {
        "Academy" -> {
            println("Tampilkan halaman ACADEMY")
        }
        "Help" -> {
            println("Tampilkan halaman HELP")
        }
        "Profile" -> {
            println("Tamilkan halaman PROFILE")
        }
        else -> {
            println("Tampilkan halaman ACADEMY")
        }
    }

// WHILE

while (kondisi) {
	
}

do {
        
} while (kondisi)

