fun main(args: Array<String>) {

    val data = listOf(1,2,3,4,5,6,7,8,9,10)
//    tampilkanAngkaGenap(data)
//    tampilkanNilaiHuruf(8)

    val fruitsArray: Array<String> // size fix, tapi data nya bisa diubah
//    val fruitsList: List<String> // size fix, data fix
    val fruitsMutableList: MutableList<String> // dinamis

    val fruits: Array<String> = arrayOf("Mangga", "Jeruk", "Pisang")
    fruits[1] = "Semangka"
//    fruits[3] = "Melon" error

    val fruitList: List<String> = listOf("Mangga", "Jeruk", "Pisang")
//    fruitList[1] = "Semangka" error
//    fruitList.add("Semangka") error

    val fruitMutableList = mutableListOf("Mangga", "Jeruk", "Pisang")
    fruitMutableList[2] = "Melon"
    fruitMutableList.add("Semangka")

//    print(fruits[3]) error

//    tampilkanBuahBuahan(fruitList)
//    tampilkanBuahBuahanTerbalik(fruitList)
//    tampilkanBuahSelainJeruk(fruitList)

//    val makanan = "Soto"
//
//    when (makanan) {
//        "Soto" -> println("Saya suka soto")
//        "Bakso" -> println("Iuuuuhhhhh")
//        else -> println("Ya boleh lah kalo $makanan")
//    }



//    tampilkanBuahbuahanDenganWhile(fruitList)

//    tampilkanBuahbuahanDenganDoWhile(fruitList)

    onOptionSelect("Academy")
}

fun tampilkanBuahbuahanDenganWhile(fruits: List<String>) {
    var i = 0
    while (i < fruits.size) {
        println(fruits[i])
        i += 1
    }
}

fun onOptionSelect(userSelect: String) {
    when(userSelect) {
        "Academy" -> println("Tampilkan halaman ACADEMY")
        "Help" -> println("Tampilkan halaman HELP")
        "Profile" -> println("Tamilkan halaman PROFILE")
        else -> println("Tampilkan halaman ACADEMY")

    }
}

fun tampilkanBuahbuahanDenganDoWhile(fruits: List<String>) {
    var i = 0
    do {
        println(fruits[i])
        i++
    } while (i < fruits.size)
}



fun tampilkanBuahBuahan(fruits: List<String>) {
    fruits.forEach {
        println(it)
    }
}

fun tampilkanBuahSelainJeruk(fruits: List<String>) {
    fruits.forEach {
        if (it != "Jeruk") {
            println(it)
        }
    }
}

fun tampilkanBuahBuahanTerbalik(fruits: List<String>) {
    for (i in fruits.size-1 downTo 0) {
        println(fruits[i])
    }
}

fun tampilkanNilaiHuruf(nilai: Int) {
    if (nilai >= 8 && nilai <= 10) {
        println("A")
    } else if (nilai >= 6) {
        println("B")
    } else if (nilai >= 4) {
        println("C")
    } else if (nilai >= 0) {
        println("D")
    }
}

fun tampilkanAngkaGenap(data: List<Int>) {
    data.forEach {
        if (it % 2 == 0) {
            println("Angka genap : $it")
        } else {
            println("Angka ganjil : $it")
        }
    }
}